import { extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import { messages } from 'vee-validate/dist/locale/en.json';

Object.keys(rules).forEach((rule) => {
    extend(rule, { ...rules[rule], message: messages[rule] });
});

extend('odd', {
    validate: value => value % 2 === 0,
});

extend('email', {
    message: 'Поле {_field_} должно быть действительным emailom ',
});

extend('positive', value => value >= 0);

extend('min', {
    validate(value, args) {
        if (!value) return false;
        return value.length >= args.length;
    },
    params: ['length'],
});

extend('minmax', {
    validate(value, args) {
        const { length } = value;
        return length >= args.min && length <= args.max;
    },
    params: ['min', 'max'],
    message: 'The {_field_} field must have at least {min} characters and {max} characters at most, current: {_value_} ',
});

extend('one_of', (value, values) => {
    console.log('Не отдекомпозированый', values);
    return values.indexOf(value) !== -1;
});

extend('negative', (value) => {
    if (value < 0) {
        return true;
    }
    return 'The {_field_} field must be a positive number';
});
