import Vue from 'vue';
import { ValidationProvider, ValidationObserver } from 'vee-validate';
import './validation';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
