export default {
    name: 'Reactive',
    created() {
        this.getAllPeople();
    },
    data() {
        return {
            persons: [],
            search: '',
            randomPerson: null,
        };
    },
    computed: {
        countPersons() {
            return this.persons.length;
        },
        findPersons() {
            if (this.search) {
                return this.persons.filter(person => person.name.toLowerCase().includes(this.search.toLowerCase()));
            }
            return [];
        },
    },
    watch: {
        // persons(list, oldvalue) {
        //     console.log(list, oldvalue);
        //     this.randomPerson = list[Math.floor(Math.random() * 10)];
        // },
        persons: {
            immediate: true,
            deep: true,
            handler(list, oldList) {
                console.log(list, oldList);
                this.randomPerson = list[Math.floor(Math.random() * 10)];
            },
        },
    },
    methods: {
        extractId(item) {
            const idRegExp = /\/([0-9]*)\/$/;
            return item.url.match(idRegExp)[1];
        },
        transformPerson(person) {
            return {
                id: this.extractId(person),
                name: person.name,
                gender: person.gender,
                birthYear: person.birth_year,
                eyeColor: person.eye_color,
            };
        },
        async getAllPeople() {
            const { data } = await this.axios.get('https://swapi.co/api/people/');
            this.persons = data.results.map(item => this.transformPerson(item));
        },
        async getPerson(id) {
            const { data } = await this.axios.get(`https://swapi.co/api/people/${id}/`);
            this.persons = data.results.map(item => this.transformPerson(item));
        },
    },
};
