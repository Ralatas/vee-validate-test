import Test from '@/components/test/test.vue';

export default {
    name: 'Validator',
    data() {
        return {
            email: null,
            value: null,
        };
    },
    methods: {
        onSubmit() {
            alert('Форма отправилась');
        },
    },
    components: {
        Test
    }
};
